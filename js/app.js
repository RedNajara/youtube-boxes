"use strict";

//Example URL for test
//?id=sw-l5SW5hik%202jSWMme12ik,p8npDG2ulKQ,pl-DjiO8das,CJRw18MtRPg%20Wpm07-BGJnE,xf4iv4ic70M,6Q0AazVu1Tc,Xyb1fsPG-Xk,kG41zm8HGSE,RxvcH25WThg,df7PZIVe1lw,sYvH7Y16iUM,juTa0fPI22M,2KuqjW0WtZg,dKccvk36atQ,Duc3F700lgE,TI5bEf-BULU,sxyotaytAS0,UZ47aQFp2TQ,qD2yyikDcDw,O4_JNAFClFk,iJz5jURaEBc,RBbkCEHBw_I,CX11yw6YL1w

/**
 * Get ids from URL
 * @param  {id}
 */
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

var idParams = getUrlVars()["id"];
var videoContainer = document.getElementById("main-container");

function hidePreloader() {
    document.querySelector("body").classList.remove("loading");
};

if (typeof idParams === "undefined" || idParams.length < 1) {
    videoContainer.innerHTML = "Please enter parameters for the videos";
    hidePreloader();
} else {

    /**
     * Create the array of urls
     */
    var urlsList = idParams.replace(/%20/g, " ").split(/[\s,]+/);

    /**
     * Get infrormation about videos
     * @param  {url}
     */
    urlsList.map(function (url) {
        var xhr = new XMLHttpRequest();

        xhr.onreadystatechange = function() {
            if (xhr.readyState != 4) return;

            if (xhr.status != 200) {
                console.log(xhr.status + ': ' + xhr.statusText);
            } else {
                try {
                    var response = JSON.parse(xhr.responseText);
                } catch (e) {
                    console.log(e.message);
                }
                pasteTitles(url,response);
            }
        };


        xhr.open('GET', "http://noembed.com/embed?url=http://www.youtube.com/watch?v=" + url);
        xhr.send();
    });

    /**
     * Paste titles for videos
     */
    function pasteTitles(url,response) {
        var titleText = "";

        if (response.title) {
            titleText = response.title;
        } else {
            titleText = response.error;
        }

        document.getElementById(url).innerHTML = titleText;
    }


    /**
     * Video markup
     */
    var videoMarkup = "\n                ".concat(urlsList.map(function (item) {
        return "<div class=\"b-coll\" data-url=\"".concat(item, "\">\n\n            <div class=\"b-wrap-img\">\n                    <img src=\"http://i.ytimg.com/vi/").concat(item, "/hqdefault.jpg\" />\n\n                    <div class=\"b-video-title\" id=\"".concat(item, "\"></div>\n                </div>\n                <div class=\"b-wrap-video\"></div>\n            </div>"));
    }).join(""));

    hidePreloader();

    /**
     * Add list of videos to the page
     */
    videoContainer.innerHTML = videoMarkup;
}

/**
 * Animation for hiding image
 */
function animateImageHide(e) {
    e.classList.add("animation-hide");
};

/**
 * Animation for showing image
 */
function animateImageShow(e) {
    for (var i = 0, len = e.length; i < len; i++) {
        e[i].classList.remove("animation-hide");
    }
};

/**
 * Remove function for IE11
 */
Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
};

/**
 * Click on image
 */
document.addEventListener("click", function (e) {
    var target = e.target;

    while (target && target.parentNode !== document) {
        target = target.parentNode;
        if (!target) { return; }

        if (target.classList.contains('b-wrap-img')){
            var thumbnails = document.querySelectorAll(".b-coll");
            var thumbnailsWrapper = target.parentNode;

            var urlAttribute = thumbnailsWrapper.getAttribute("data-url");
            var videoWrapper = thumbnailsWrapper.querySelectorAll(".b-wrap-video")[0];
            var videoBlock = document.getElementsByClassName("b-video-iframe")[0];
            var videoIframe = "<div class=\"b-video-iframe\"><iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/".concat(urlAttribute, "?autoplay=1\" frameborder=\"0\" allowfullscreen allow=\"autoplay\" allowscriptaccess=\"always\"></iframe></div>");
            animateImageShow(thumbnails);
            animateImageHide(thumbnailsWrapper);

            if (videoBlock) {
                videoBlock.remove();
            }

            videoWrapper.innerHTML = videoIframe;
        }
    }
});